package com.crsys.test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.Test;

import com.crsys.bean.User;

public class UserTest {

	@Test
	public void testGetRole() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final User testUser = new User();
        final Field field = testUser.getClass().getDeclaredField("role");
        field.setAccessible(true);
        field.set(testUser, "a");
        final String result = testUser.getRole();
        assertEquals("field wasn't retrieved properly", result, "a");
	}

	@Test
	public void testToString() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final User testUser = new User();
        final Field field = testUser.getClass().getDeclaredField("u_name");
        field.setAccessible(true);
        field.set(testUser, "ali");
        final String result = testUser.toString();
        assertEquals("field wasn't retrieved properly", result, "ali");
	}

	@Test
	public void testSetRole() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final User testUser=new User();
		String role="aas";
		testUser.setRole(role);
		final Field field= testUser.getClass().getDeclaredField("role");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testUser), role);
	}

	@Test
	public void testGetPassword() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final User testUser = new User();
        final Field field = testUser.getClass().getDeclaredField("password");
        field.setAccessible(true);
        field.set(testUser, "a");
        final String result = testUser.getPassword();
        assertEquals("field wasn't retrieved properly", result, "a");
	}

	@Test
	public void testSetPassword() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final User testUser=new User();
		String password="aas";
		testUser.setPassword(password);
		final Field field= testUser.getClass().getDeclaredField("password");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testUser), password);
	}

	@Test
	public void testUser() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final User testUser=new User();
		String word="aas";
		testUser.setName(word);
		testUser.setSurname(word);
		testUser.setAddress(word);
		testUser.setU_name(word);
		final Field nfield= testUser.getClass().getDeclaredField("name");
		nfield.setAccessible(true);
		final Field sfield= testUser.getClass().getDeclaredField("surname");
		sfield.setAccessible(true);
		final Field afield= testUser.getClass().getDeclaredField("address");
		afield.setAccessible(true);
		final Field ufield= testUser.getClass().getDeclaredField("u_name");
		ufield.setAccessible(true);
		assertEquals("Fields didn't match",nfield.get(testUser), word);
		assertEquals("Fields didn't match",sfield.get(testUser), word);
		assertEquals("Fields didn't match",afield.get(testUser), word);
		assertEquals("Fields didn't match",ufield.get(testUser), word);
	}

	@Test
	public void testGetName() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final User testUser = new User();
        final Field field = testUser.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(testUser, "a");
        final String result = testUser.getName();
        assertEquals("field wasn't retrieved properly", result, "a");
	}

	@Test
	public void testSetName() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final User testUser=new User();
		String name="aas";
		testUser.setName(name);
		final Field field= testUser.getClass().getDeclaredField("name");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testUser), name);
	}

	@Test
	public void testGetSurname() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final User testUser = new User();
        final Field field = testUser.getClass().getDeclaredField("surname");
        field.setAccessible(true);
        field.set(testUser, "a");
        final String result = testUser.getSurname();
        assertEquals("field wasn't retrieved properly", result, "a");
	}

	@Test
	public void testSetSurname() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final User testUser=new User();
		String surname="aas";
		testUser.setSurname(surname);
		final Field field= testUser.getClass().getDeclaredField("surname");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testUser), surname);
	}

	@Test
	public void testGetAddress() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final User testUser = new User();
        final Field field = testUser.getClass().getDeclaredField("address");
        field.setAccessible(true);
        field.set(testUser, "a");
        final String result = testUser.getAddress();
        assertEquals("field wasn't retrieved properly", result, "a");
	}

	@Test
	public void testSetAddress() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final User testUser=new User();
		String address="aas";
		testUser.setAddress(address);
		final Field field= testUser.getClass().getDeclaredField("address");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testUser), address);
	}

	@Test
	public void testGetU_name() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final User testUser = new User();
        final Field field = testUser.getClass().getDeclaredField("u_name");
        field.setAccessible(true);
        field.set(testUser, "a");
        final String result = testUser.getU_name();
        assertEquals("field wasn't retrieved properly", result, "a");
	}

	@Test
	public void testSetU_name() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final User testUser=new User();
		String u_name="aas";
		testUser.setU_name(u_name);
		final Field field= testUser.getClass().getDeclaredField("u_name");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testUser), u_name);
	}

}
