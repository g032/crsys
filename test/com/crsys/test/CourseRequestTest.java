package com.crsys.test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.Test;

import com.crsys.bean.CourseRequest;



public class CourseRequestTest {

	@Test
	public void testGetInstructor() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final CourseRequest testCourseRequest = new CourseRequest();
        final Field field = testCourseRequest.getClass().getDeclaredField("instructor");
        field.setAccessible(true);
        field.set(testCourseRequest, "a");
        final String result = testCourseRequest.getInstructor();
        assertEquals("field wasn't retrieved properly", result, "a");
	}

	@Test
	public void testSetInstructor() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final CourseRequest testCourseRequest=new CourseRequest();
		String instructor="aas";
		testCourseRequest.setInstructor(instructor);
		final Field field= testCourseRequest.getClass().getDeclaredField("instructor");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testCourseRequest), instructor);
	}

	@Test
	public void testGetCourseName() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final CourseRequest testCourseRequest = new CourseRequest();
        final Field field = testCourseRequest.getClass().getDeclaredField("courseName");
        field.setAccessible(true);
        field.set(testCourseRequest, "a");
        final String result = testCourseRequest.getCourseName();
        assertEquals("field wasn't retrieved properly", result, "a");
	}

	@Test
	public void testSetCourseName() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final CourseRequest testCourseRequest=new CourseRequest();
		String coursename="aas";
		testCourseRequest.setCourseName(coursename);
		final Field field= testCourseRequest.getClass().getDeclaredField("courseName");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testCourseRequest), coursename);
	}

	@Test
	public void testGetCourseCRN() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final CourseRequest testCourseRequest = new CourseRequest();
        final Field field = testCourseRequest.getClass().getDeclaredField("courseCRN");
        field.setAccessible(true);
        field.set(testCourseRequest, 1);
        final int result = testCourseRequest.getCourseCRN();
        assertEquals("field wasn't retrieved properly", result, 1);
	}

	@Test
	public void testSetCourseCRN() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final CourseRequest testCourseRequest=new CourseRequest();
		int coursecrn=1;
		testCourseRequest.setCourseCRN(coursecrn);
		final Field field= testCourseRequest.getClass().getDeclaredField("courseCRN");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testCourseRequest), coursecrn);
	}

}
