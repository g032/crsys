package com.crsys.test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.crsys.bean.Course;

public class CourseTest {

	@Test
	public void testGetRemaining() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Course testCourse = new Course();
        final Field field = testCourse.getClass().getDeclaredField("remaining");
        field.setAccessible(true);
        field.set(testCourse, 5);
        final int result = testCourse.getRemaining();

        assertEquals("field wasn't retrieved properly", result, 5);
	}

	@Test
	public void testSetRemaining() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Course testCourse=new Course();
		int rem=5;
		testCourse.setRemaining(rem);
		final Field field= testCourse.getClass().getDeclaredField("remaining");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testCourse), 5);
	}

	@Test
	public void testGetIns() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Course testCourse = new Course();
        final Field field = testCourse.getClass().getDeclaredField("ins");
        field.setAccessible(true);
        field.set(testCourse, "lorem ipsum");
        final String result = testCourse.getIns();

        assertEquals("field wasn't retrieved properly", result, "lorem ipsum");
	}

	@Test
	public void testSetIns() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Course testCourse=new Course();
		String ins="lorem ipsum";
		testCourse.setIns(ins);
		final Field field= testCourse.getClass().getDeclaredField("ins");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testCourse), ins);
	}

	@Test
	public void testCourse() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Course testCourse=new Course();
		final Field namefield= testCourse.getClass().getDeclaredField("name");
		namefield.setAccessible(true);
		final Field crnfield=testCourse.getClass().getDeclaredField("crn");
		crnfield.setAccessible(true);
		assertEquals("Fields didn't match",namefield.get(testCourse), "");
		assertEquals("Fields didn't match",crnfield.get(testCourse), -1);
	}

	@Test
	public void testGetName() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Course testCourse = new Course();
        final Field field = testCourse.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(testCourse, "lorem ipsum");
        final String result = testCourse.getName();
        assertEquals("field wasn't retrieved properly", result, "lorem ipsum");
	}

	@Test
	public void testSetName() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Course testCourse=new Course();
		String name="lorem ipsum";
		testCourse.setName(name);
		final Field field= testCourse.getClass().getDeclaredField("name");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testCourse), name);
	}

	@Test
	public void testGetCrn() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Course testCourse = new Course();
        final Field field = testCourse.getClass().getDeclaredField("crn");
        field.setAccessible(true);
        field.set(testCourse, 1);
        final int result = testCourse.getCrn();
        assertEquals("field wasn't retrieved properly", result, 1);
	}

	@Test
	public void testSetCrn() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Course testCourse=new Course();
		int crn=1;
		testCourse.setCrn(crn);
		final Field field= testCourse.getClass().getDeclaredField("crn");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testCourse), crn);
	}

	@Test
	public void testToString() {
		final Course testCourse=new Course();
		assertEquals("Strings didn't match",testCourse.toString(),"-1 ");
	}

	@Test
	public void testGetStart() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Course testCourse = new Course();
        final Field field = testCourse.getClass().getDeclaredField("start");
        field.setAccessible(true);
        Date d=new Date(2000,1,1,0,0);
        field.set(testCourse, d);
        final Date result = testCourse.getStart();
        assertEquals("field wasn't retrieved properly", result, d);
	}


	@Test
	public void testGetEnd() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Course testCourse = new Course();
        final Field field = testCourse.getClass().getDeclaredField("end");
        field.setAccessible(true);
        Date d=new Date(2000,1,1,0,0);
        field.set(testCourse, d);
        final Date result = testCourse.getEnd();
        assertEquals("field wasn't retrieved properly", result, d);
	}

	
}
