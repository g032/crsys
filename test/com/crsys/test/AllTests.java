package com.crsys.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CourseRequestTest.class, CourseTest.class, DatabaseTest.class, GradeTest.class,
		SpecialPermissionTest.class, UserTest.class })
public class AllTests {

}
