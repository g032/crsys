package com.crsys.test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.Test;

import com.crsys.bean.SpecialPermission;

public class SpecialPermissionTest {

	@Test
	public void testSpecialPermission() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final SpecialPermission testPermission=new SpecialPermission();
		final Field efield= testPermission.getClass().getDeclaredField("explanation");
		final Field cfield= testPermission.getClass().getDeclaredField("crn");
		
		efield.setAccessible(true);
		cfield.setAccessible(true);
		assertEquals("Fields didn't match",efield.get(testPermission), "");
		assertEquals("Fields didn't match",cfield.get(testPermission), -1);
	}

	@Test
	public void testGetExplanation() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final SpecialPermission testPermission = new SpecialPermission();
        final Field field = testPermission.getClass().getDeclaredField("explanation");
        field.setAccessible(true);
        field.set(testPermission, "lorem ipsum");
        final String result = testPermission.getExplanation();

        assertEquals("field wasn't retrieved properly", result, "lorem ipsum");
	}

	@Test
	public void testSetExplanation() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final SpecialPermission testPermission=new SpecialPermission();
		String exp="asd";
		testPermission.setExplanation(exp);
		final Field field= testPermission.getClass().getDeclaredField("explanation");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testPermission), exp);
	}

	@Test
	public void testGetCrn() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final SpecialPermission testPermission = new SpecialPermission();
        final Field field = testPermission.getClass().getDeclaredField("crn");
        field.setAccessible(true);
        field.set(testPermission, 1);
        final int result = testPermission.getCrn();

        assertEquals("field wasn't retrieved properly", result, 1);
	}

	@Test
	public void testSetCrn() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final SpecialPermission testPermission=new SpecialPermission();
		int crn=1;
		testPermission.setCrn(crn);
		final Field field= testPermission.getClass().getDeclaredField("crn");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testPermission), crn);
	}

	@Test
	public void testGetStudentUname() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final SpecialPermission testPermission = new SpecialPermission();
        final Field field = testPermission.getClass().getDeclaredField("studentUname");
        field.setAccessible(true);
        field.set(testPermission, "aaa");
        final String result = testPermission.getStudentUname();

        assertEquals("field wasn't retrieved properly", result, "aaa");
	}

	@Test
	public void testSetStudentUname() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final SpecialPermission testPermission=new SpecialPermission();
		String u="aaaaa";
		testPermission.setStudentUname(u);
		final Field field= testPermission.getClass().getDeclaredField("studentUname");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testPermission), "aaaaa");
	}

	@Test
	public void testToString() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final SpecialPermission testPermission = new SpecialPermission();
        final Field sfield = testPermission.getClass().getDeclaredField("studentUname");
        final Field cfield = testPermission.getClass().getDeclaredField("crn");
        final Field efield = testPermission.getClass().getDeclaredField("explanation");
        sfield.setAccessible(true);
        sfield.set(testPermission, "aaa");
        cfield.setAccessible(true);
        cfield.set(testPermission, 1);
        efield.setAccessible(true);
        efield.set(testPermission, "aaa");
        final String sresult = testPermission.getStudentUname();
        final int cresult = testPermission.getCrn();
        final String eresult = testPermission.getExplanation();
        assertEquals("field wasn't retrieved properly", sresult, "aaa");
        assertEquals("field wasn't retrieved properly", cresult, 1);
        assertEquals("field wasn't retrieved properly", eresult, "aaa");
	}

}
