package com.crsys.test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.Test;

import com.crsys.db.DatabaseOperations;
import com.mysql.jdbc.Statement;


public class DatabaseTest {

	@Test
	public void testInitialize() throws NoSuchFieldException, SecurityException, SQLException, IllegalArgumentException, IllegalAccessException {
		final DatabaseOperations db=new DatabaseOperations();
		db.initialize();
		final Field field = db.getClass().getDeclaredField("con");
		field.setAccessible(true);
		Boolean a=((Connection) field.get(db)).isClosed();
		assertFalse("database is not initialized properly", a);	
	}
	
	@Test
	public void testFinalize() throws NoSuchFieldException, SecurityException, SQLException, IllegalArgumentException, IllegalAccessException {
		final DatabaseOperations db=new DatabaseOperations();
		db.finalize();
		final Field field = db.getClass().getDeclaredField("con");
		field.setAccessible(true);
		assertEquals("database is not closed properly", field.get(db), null);
	}
	
	@Test
	public void testEditInfo() throws NoSuchFieldException, SecurityException, SQLException, IllegalArgumentException, IllegalAccessException {
		final DatabaseOperations db=new DatabaseOperations();
		db.editInfo("name","surname","adress","u_name");
		Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/crsys","root","");
		String queryN = "SELECT firstname " +
						"FROM user_info";
		String queryS= "SELECT surname " +
					   "FROM user_info";
		String queryA = "SELECT address " +
				        "FROM user_info";
		String queryU = "SELECT u_name " +
				        "FROM user_info";
		java.sql.Statement stmtN = null;
		java.sql.Statement stmtS = null;
		java.sql.Statement stmtA = null;
		java.sql.Statement stmtU = null;
		stmtN = connection.createStatement();
		stmtS = connection.createStatement();
		stmtA = connection.createStatement();
		stmtU = connection.createStatement();
		ResultSet rsN=stmtN.executeQuery(queryN);
		ResultSet rsS=stmtS.executeQuery(queryS);
		ResultSet rsA=stmtA.executeQuery(queryA);
		ResultSet rsU=stmtU.executeQuery(queryU);
		
		assertEquals("Name is not updated properly", stmtN.getResultSet(), rsN);
		assertEquals("Surname is not updated properly", stmtS.getResultSet(), rsS);
		assertEquals("Adress is not updated properly", stmtA.getResultSet(), rsA);
		assertEquals("User name is not updated properly", stmtU.getResultSet(), rsU);

	}

}