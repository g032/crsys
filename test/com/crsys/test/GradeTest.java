package com.crsys.test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;

import org.junit.Test;

import com.crsys.bean.Grade;

public class GradeTest {

	@Test
	public void testGetTerm() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Grade testGrade = new Grade();
        final Field field = testGrade.getClass().getDeclaredField("term");
        field.setAccessible(true);
        field.set(testGrade, 2000);
        final int result = testGrade.getTerm();
        assertEquals("field wasn't retrieved properly", result, 2000);
	}

	@Test
	public void testSetTerm() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Grade testGrade=new Grade();
		int term=5;
		testGrade.setTerm(term);
		final Field field= testGrade.getClass().getDeclaredField("term");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testGrade), term);
	}

	@Test
	public void testGetCoursename() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Grade testGrade = new Grade();
        final Field field = testGrade.getClass().getDeclaredField("coursename");
        field.setAccessible(true);
        field.set(testGrade, "dolor sit amet");
        final String result = testGrade.getCoursename();
        assertEquals("field wasn't retrieved properly", result, "dolor sit amet");
	}

	@Test
	public void testSetCoursename() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Grade testGrade=new Grade();
		String coursename="lorem ipsum";
		testGrade.setCoursename(coursename);
		final Field field= testGrade.getClass().getDeclaredField("coursename");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testGrade), coursename);
	}

	@Test
	public void testGetStudentName() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Grade testGrade = new Grade();
        final Field field = testGrade.getClass().getDeclaredField("studentName");
        field.setAccessible(true);
        field.set(testGrade, "dolor sit amet");
        final String result = testGrade.getStudentName();
        assertEquals("field wasn't retrieved properly", result, "dolor sit amet");
	}

	@Test
	public void testSetStudentName() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Grade testGrade=new Grade();
		String studentname="lorem ipsum";
		testGrade.setStudentName(studentname);
		final Field field= testGrade.getClass().getDeclaredField("studentName");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testGrade), studentname);
	}

	@Test
	public void testGetStudentUname() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Grade testGrade = new Grade();
        final Field field = testGrade.getClass().getDeclaredField("studentUname");
        field.setAccessible(true);
        field.set(testGrade, "dolor sit amet");
        final String result = testGrade.getStudentUname();
        assertEquals("field wasn't retrieved properly", result, "dolor sit amet");
	}

	@Test
	public void testSetStudentUname() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Grade testGrade=new Grade();
		String studentuname="lorem ipsum";
		testGrade.setStudentUname(studentuname);
		final Field field= testGrade.getClass().getDeclaredField("studentUname");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testGrade), studentuname);
	}

	@Test
	public void testGetGrade() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Grade testGrade = new Grade();
        final Field field = testGrade.getClass().getDeclaredField("grade");
        field.setAccessible(true);
        field.set(testGrade, "AAA");
        final String result = testGrade.getGrade();
        assertEquals("field wasn't retrieved properly", result, "AAA");
	}

	@Test
	public void testSetGrade() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Grade testGrade=new Grade();
		String grade="AAA";
		testGrade.setGrade(grade);
		final Field field= testGrade.getClass().getDeclaredField("grade");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testGrade), grade);
	}

	@Test
	public void testGetCrn() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		final Grade testGrade = new Grade();
        final Field field = testGrade.getClass().getDeclaredField("crn");
        field.setAccessible(true);
        field.set(testGrade,1);
        final int result = testGrade.getCrn();
        assertEquals("field wasn't retrieved properly", result, 1);
	}

	@Test
	public void testSetCrn() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		final Grade testGrade=new Grade();
		int crn=1;
		testGrade.setCrn(crn);
		final Field field= testGrade.getClass().getDeclaredField("crn");
		field.setAccessible(true);
		assertEquals("Fields didn't match",field.get(testGrade), crn);
	}

	@Test
	public void testToString() {
		final Grade testGrade=new Grade();
		testGrade.setCrn(0);
		testGrade.setGrade("AAA");
		testGrade.setStudentUname("ali");
		assertEquals("Strings didn't match",testGrade.toString(),"0 AAA ali");
	}

}
