CREATE DATABASE crsys;
use crsys
CREATE TABLE login
(
	id int AUTO_INCREMENT UNIQUE,
	u_name VARCHAR(16),
	u_pass VARCHAR(32) NOT NULL,
	u_role VARCHAR(5) NOT NULL DEFAULT 'stu',
	PRIMARY KEY(u_name)
);
CREATE TABLE course
(
	c_crn int AUTO_INCREMENT,
	c_name VARCHAR(255) NOT NULL,
	c_capacity int,
	c_start DATETIME,
	c_end DATETIME,
	c_ins VARCHAR(16),
	PRIMARY KEY(c_crn),
	FOREIGN KEY(c_ins) REFERENCES login(u_name) ON DELETE CASCADE
);
CREATE TABLE corequisite
(
	c_crn int,
	co_crn int,
	PRIMARY KEY(c_crn,co_crn),
	FOREIGN KEY(c_crn) REFERENCES course(c_crn) ON DELETE CASCADE,
	FOREIGN KEY(co_crn) REFERENCES course(c_crn) ON DELETE CASCADE
);
CREATE TABLE prerequisite
(
	c_crn int,
	pre_crn int,
	PRIMARY KEY(c_crn,pre_crn),
	FOREIGN KEY(c_crn) REFERENCES course(c_crn) ON DELETE CASCADE,
	FOREIGN KEY(pre_crn) REFERENCES course(c_crn) ON DELETE CASCADE
);
CREATE TABLE specialapproval
(
	u_name VARCHAR(16) NOT NULL,
	c_crn int NOT NULL,
	PRIMARY KEY(u_name,c_crn),
	FOREIGN KEY(u_name) REFERENCES login(u_name) ON DELETE CASCADE,
	FOREIGN KEY(c_crn) REFERENCES course(c_crn) ON DELETE CASCADE
);
CREATE TABLE specialrequest
(
	u_name VARCHAR(16) NOT NULL,
	c_crn int NOT NULL,
	message VARCHAR(255) NOT NULL,
	PRIMARY KEY(u_name,c_crn),
	FOREIGN KEY(u_name) REFERENCES login(u_name) ON DELETE CASCADE,
	FOREIGN KEY(c_crn) REFERENCES course(c_crn) ON DELETE CASCADE
);
CREATE TABLE user_info
(
	u_name VARCHAR(16),
	firstname VARCHAR(255) NOT NULL,
	surname VARCHAR(255) NOT NULL,
	address VARCHAR(255) NOT NULL,	
	PRIMARY KEY(u_name),
	FOREIGN KEY(u_name) REFERENCES login(u_name)ON DELETE CASCADE
);
CREATE TABLE course_req
(
	co_crn int,
	pre_crn int,
	c_name VARCHAR(255) NOT NULL,
	capacity int NOT NULL,
	c_start DATETIME,
	c_end DATETIME,
	req_ins VARCHAR(16),
	FOREIGN KEY(req_ins) REFERENCES login(u_name) ON DELETE CASCADE
);
CREATE TABLE course_app
(
	u_name VARCHAR(16),
	c_crn int,
	PRIMARY KEY(u_name,c_crn),
	FOREIGN KEY(c_crn) REFERENCES course(c_crn),
	FOREIGN KEY(u_name) REFERENCES login(u_name) ON DELETE CASCADE

);
CREATE TABLE transcript
(
	u_name VARCHAR(16),
	c_crn int,
	term_year  YEAR NOT NULL,
	grade VARCHAR(2) NOT NULL,
	PRIMARY KEY(u_name,c_crn),
	FOREIGN KEY(c_crn) REFERENCES course(c_crn),
	FOREIGN KEY(u_name) REFERENCES login(u_name) ON DELETE CASCADE
); 
CREATE TABLE current_course
(
	u_name VARCHAR(16),
	c_crn int,
	grade VARCHAR(2),
	PRIMARY KEY(u_name,c_crn),
	FOREIGN KEY(c_crn) REFERENCES course(c_crn),
	FOREIGN KEY(u_name) REFERENCES login(u_name) ON DELETE CASCADE
);