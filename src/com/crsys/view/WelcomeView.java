package com.crsys.view;

import javax.faces.bean.ManagedBean;

import com.crsys.bean.SessionBean;
import com.crsys.bean.User;
import com.crsys.db.DatabaseOperations;
import com.crsys.db.StudentOperations;

@ManagedBean(name="WelcomeView")
public class WelcomeView {

	private String name;
	private String role;
	private String fullname;
	private String nname,nsurname,naddress;
	public String getNname() {
		return nname;
	}


	public void setNname(String nname) {
		this.nname = nname;
	}


	public String getNsurname() {
		return nsurname;
	}


	public void setNsurname(String nsurname) {
		this.nsurname = nsurname;
	}


	public String getNaddress() {
		return naddress;
	}


	public void setNaddress(String naddress) {
		this.naddress = naddress;
	}

	private String address;
	private Boolean stu,inst,root,sysadmin;
	public WelcomeView()
	{
		StudentOperations db = new StudentOperations();
		db.initialize();
		User current = new User();
		current=db.getInfo(SessionBean.getUserName());
		fullname= current.getName()+ " "+current.getSurname();
		name=SessionBean.getUserName();
		address = current.getAddress();
		role = SessionBean.getUserRole();
		stu=false;inst=false;root=false;sysadmin=false;
		if(role.equals("stu"))
		{
			stu=true;
			role="Student";
		}
		else if(role.equals("root"))
		{
			root=true;
			role="Root";
		}
		else if(role.equals("ins"))
		{
			inst=true;
			role="Instructor";
		}
		else if(role.equals("admin"))
		{
			sysadmin=true;
			role="System Administrator";
		}
	}

	
	public void editInfo(){
		System.out.println(nname);
		System.out.println(nsurname);
		System.out.println(naddress);
		DatabaseOperations db = new DatabaseOperations();
		db.editInfo(nname, nsurname, naddress, SessionBean.getUserName());
	}
	public Boolean getStu() {
		return stu;
	}



	public void setStu(Boolean stu) {
		this.stu = stu;
	}



	public Boolean getInst() {
		return inst;
	}

	public void setInst(Boolean inst) {
		this.inst = inst;
	}



	public Boolean getRoot() {
		return root;
	}



	public void setRoot(Boolean root) {
		this.root = root;
	}



	public Boolean getSysadmin() {
		return sysadmin ;
	}



	public void setSysadmin(Boolean sysadmin) {
		this.sysadmin = sysadmin;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
