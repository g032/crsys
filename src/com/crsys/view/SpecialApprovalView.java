package com.crsys.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import com.crsys.bean.*;
import com.crsys.db.InstructorOperations;

@ManagedBean(name="SpecialApprovalView")
public class SpecialApprovalView {
	private List<SpecialPermission> requests;
	private SpecialPermission selectedRequest;
	
	public List<SpecialPermission> getRequests() {
		return requests;
	}

	public void setRequests(List<SpecialPermission> requests) {
		this.requests = requests;
	}

	public SpecialPermission getSelectedRequest() {
		return selectedRequest;
	}

	public void setSelectedRequest(SpecialPermission selectedRequest) {
		this.selectedRequest = selectedRequest;
	}
	public void accept(){
		System.out.println(selectedRequest.getStudentUname());
		InstructorOperations db = new InstructorOperations();
		db.approveRequest(selectedRequest.getStudentUname(), selectedRequest.getCrn());
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Special Permission Request Accepted","Please try again"));
		init();
	}
	public void reject(){
		System.out.println(selectedRequest.getStudentUname());
		InstructorOperations db = new InstructorOperations();
		db.rejectRequest(selectedRequest.getStudentUname(), selectedRequest.getCrn());
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Special Permission Request Rejected","Please try again"));
		init();
	}

	@PostConstruct
	public void init()
	{
		System.out.println("initteyim");
		InstructorOperations db = new InstructorOperations();
		requests=db.getPermissionRequests(SessionBean.getUserName());
	}
}
