package com.crsys.view;
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.crsys.bean.SessionBean;
import com.crsys.db.*;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

@ManagedBean(name="LoginView")
@SessionScoped
public class LoginView {
     
	private String username;
	private String password;
	
	 public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String login()
		{
			System.out.println(username +" "+password);
			LoginOperations db = new LoginOperations();
			db.initialize();
			FacesContext context = FacesContext.getCurrentInstance();
			String checkrole =db.checkCredentials(username, password);
			if(!checkrole.equals(""))
			{
				//addMessage("Successfull Login!");
				HttpSession session = SessionBean.getSession();
	            session.setAttribute("username", username);
	            session.setAttribute("userrole",checkrole);
	            return "welcome";
			}
			else
			{
				context.addMessage(null, new FacesMessage("Wrong Credentials or account may be banned","Please try again"));
			}
				return "loginpage";
		}
		public String logout() 
		{
			HttpSession session = SessionBean.getSession();
			session.invalidate();
		    return "/loginpage.xhtml";
		}
}