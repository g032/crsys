package com.crsys.view;


import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.crsys.bean.Course;
import com.crsys.bean.CourseRequest;
import com.crsys.bean.SessionBean;
import com.crsys.db.RootOperations;

@ManagedBean(name="ApproveCourseView")
@ViewScoped
public class ApproveCourseView{

	private List<CourseRequest> requests;
	private CourseRequest selectedRequest;
	public CourseRequest getSelectedRequest() {
		return selectedRequest;
	}
	public void setSelectedRequest(CourseRequest selectedRequest) {
		this.selectedRequest = selectedRequest;
	}
	@PostConstruct
	public void init() {
		RootOperations db = new RootOperations();
		requests = db.getCourseRequests();
	}
	public List<CourseRequest> getRequests() {
		return requests;
	}
	public void setRequests(List<CourseRequest> requests) {
		this.requests = requests;
	}
	public void accept(){
		RootOperations db = new RootOperations();
		db.acceptRequest(selectedRequest);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Course request has been approved",""));
	}
	public void reject(){
		RootOperations db = new RootOperations();
		db.rejectRequest(selectedRequest);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Course request has been rejected",""));
	}
}

