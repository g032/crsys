package com.crsys.view;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.crsys.bean.Course;
import com.crsys.bean.CourseRequest;
import com.crsys.bean.SessionBean;
import com.crsys.bean.User;
import com.crsys.db.InstructorOperations;
import com.crsys.db.RootOperations;
import com.crsys.db.StudentOperations;

@ManagedBean(name="CourseView")
@ViewScoped
public class CourseView {

	private Course selectedCourse;
	private List<Course> prerequisites;
	private List<Course> corequisites;
	 @ManagedProperty("#{courseService}")
	 private Course service;
	 public List<Course> getPrerequisites() {
		return prerequisites;
	}
	public void setPrerequisites(List<Course> prerequisites) {
		this.prerequisites = prerequisites;
	}
	public List<Course> getCorequisites() {
		return corequisites;
	}
	public void setCorequisites(List<Course> corequisites) {
		this.corequisites = corequisites;
	}
	private String c_name,cname;
	 public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public Date getEndtt() {
		return endtt;
	}
	public void setEndtt(Date endtt) {
		this.endtt = endtt;
	}
	public Date getStartt() {
		return startt;
	}
	public void setStartt(Date startt) {
		this.startt = startt;
	}
	private String stu;
	 private Date end,start,endtt,startt;
	private String message;
	 private int capacity;
	 public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	private List<Course> courses;
	 private List<Course> currentCourses;
	 private List<Course> filteredCourses;
	 private Course selectedCourse2;
	
	 private List<Course> addList = new ArrayList<Course>();
	 private List<Course> dropList = new ArrayList<Course>();
	 public List<Course> getDropList() {
		return dropList;
	}
	public void setDropList(List<Course> dropList) {
		this.dropList = dropList;
	}
	public Course getSelectedCourse2() {
			return selectedCourse2;
		}
	 public void setSelectedCourse2(Course selectedCourse2) {
			this.selectedCourse2 = selectedCourse2;
		}
	 public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public void request(){
			StudentOperations db = new StudentOperations();
			FacesContext context = FacesContext.getCurrentInstance();
			try {
				db.createApprovalRequest(selectedCourse.getCrn(), SessionBean.getUserName(), message);
				
				 context.addMessage(null, new FacesMessage("Special Permission Request has been sent!",""));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				
				context.addMessage(null, new FacesMessage("Permission request for this course already exists!",""));
			}
			
		}
	 public void remove(){
		 System.out.println(selectedCourse2.getCrn());
		 addList.remove(selectedCourse2);
		 FacesContext context = FacesContext.getCurrentInstance();
		 context.addMessage(null, new FacesMessage("Course removed from adding list!",""));
	}
	 public void remove2(){
		 System.out.println(selectedCourse2.getCrn());
		 dropList.remove(selectedCourse2);
		 FacesContext context = FacesContext.getCurrentInstance();
		 context.addMessage(null, new FacesMessage("Course removed from dropping list!",""));
	 }
	 public String getStu() {
		return stu;
	}
	public void setStu(String stu) {
		this.stu = stu;
	}
	
	public List<Course> getAddList() {
		return addList;
	}
	public void setAddList(List<Course> addList) {
		this.addList = addList;
	}
	public void requestCo()
	{
		System.out.println(selectedCourse.getCrn());
		System.out.println(cname);
		System.out.println(startt);
		System.out.println(endtt);
		System.out.println(capacity);
		InstructorOperations db = new InstructorOperations();
		CourseRequest cr = new CourseRequest(SessionBean.getUserName(),cname,selectedCourse.getCrn(),-1,capacity,startt,endtt);
		db.makeCourseRequest(cr);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Corequisite requisite has been sent!",""));
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public void dropList(){
		FacesContext context = FacesContext.getCurrentInstance();
		if(!dropList.contains(selectedCourse))	
		{
			dropList.add(selectedCourse);
			context.addMessage(null, new FacesMessage("Course added to Drop List",""));
		}
		else{
			context.addMessage(null, new FacesMessage("Dropping list already contains this course!",""));
		}
	}
	public String getC_name() {
		return c_name;
	}
	public void setC_name(String c_name) {
		this.c_name = c_name;
	}
	public Course getService() {
		return service;
	}
	public void setService(Course service) {
		this.service = service;
	}
	public Course getSelectedCourse() {
		return selectedCourse;
	}
	public void setSelectedCourse(Course selectedCourse) {
		this.selectedCourse = selectedCourse;
	}
	public List<Course> getFilteredCourses() {
		return filteredCourses;
	}
	public void setFilteredCourses(List<Course> filteredCourses) {
		this.filteredCourses = filteredCourses;
	}
	
	public void modify()
	{
		if(start!=null && end!=null && start.compareTo(end)>=0)
		{
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Start time has to be before end time!",""));
		}
		else
		{
			RootOperations db = new RootOperations();
			db.editCourse(selectedCourse.getCrn(), c_name, start, end);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Course Information Updated!",""));
			init();
		}
	}
	public void register()
	{
		System.out.println(stu);
		InstructorOperations db = new InstructorOperations();
		List<User> current = db.getUsers(selectedCourse.getCrn());
		boolean exist=false;
		for( int i=0;i<current.size();i++)
		{
			if(current.get(i).getU_name().equals(stu))
			{
				exist=true;
			}
		}
		if(exist){
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Student is already registered!",""));
		}else{
			RootOperations dr = new RootOperations();
			dr.registerOverride(stu, selectedCourse.getCrn());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Student has been registered!",""));
		}
	}
	public void add(){
		FacesContext context = FacesContext.getCurrentInstance();
		StudentOperations db = new StudentOperations();
		try {
			db.addCourse(addList, SessionBean.getUserName());
			context.addMessage(null, new FacesMessage("Courses added to your current Course!",""));
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage("There is a conflict in your course choices.",""));
			
		}
	}
	public void drop(){
		FacesContext context = FacesContext.getCurrentInstance();
		for(int i=0;i<dropList.size();i++)
		{
			System.out.println(dropList.get(i).getCrn());
		}
		StudentOperations db = new StudentOperations();
		try {
			db.dropCourse(SessionBean.getUserName(),dropList);
			context.addMessage(null, new FacesMessage("Courses dropped from the schedule!",""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			context.addMessage(null, new FacesMessage("Problem occured while dropping courses!",""));
		}
		init();
	}
	public void addtolist(){
		FacesContext context = FacesContext.getCurrentInstance();
		if(!addList.contains(selectedCourse))	
		{
			addList.add(selectedCourse);
			context.addMessage(null, new FacesMessage("Course added to Adding List",""));
		}
		else{
			context.addMessage(null, new FacesMessage("Adding list already contains this course!",""));
		}
		
	}
	public void delete(){
		RootOperations db = new RootOperations();
		
		db.deleteCourse(selectedCourse.getCrn());
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Course Deleted!",""));
		init();
	}
	
	public List<Course> getCurrentCourses() {
		return currentCourses;
	}
	public void setCurrentCourses(List<Course> currentCourses) {
		this.currentCourses = currentCourses;
	}
	public List<Course> getCourses() {
		return courses;
	}
	
	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
	@PostConstruct
    public void init() {
		RootOperations db = new RootOperations();
		courses= db.getCourses();
		System.out.println("sdfsd");
		StudentOperations db1 = new StudentOperations();
		if(SessionBean.getUserRole().equals("stu"))
		{
			StudentOperations db2 = new StudentOperations();
			StudentOperations db3 = new StudentOperations();
			corequisites= db3.getCorequisite();
			prerequisites=db2.getPrerequisite();
			currentCourses = db1.getSchedule(SessionBean.getUserName());
		}
	}
	
}