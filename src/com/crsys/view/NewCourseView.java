package com.crsys.view;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import com.crsys.bean.Course;
import com.crsys.bean.CourseRequest;
import com.crsys.bean.SessionBean;
import com.crsys.db.InstructorOperations;
import com.crsys.db.RootOperations;

@ManagedBean(name="NewCourseView")
public class NewCourseView {

	private Date start;
	private List<Course> courses;
	private int prereq;
	private Date end;
	private String name;
	private int capacity;
	
	
	public List<Course> getCourses() {
		return courses;
	}
	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
	public int getPrereq() {
		return prereq;
	}
	public void setPrereq(int prereq) {
		this.prereq = prereq;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	@PostConstruct
    public void init() {
		RootOperations db = new RootOperations();
		courses=db.getCourses();
	}
	public void request() {
		
		if(start.compareTo(end)>=0)
		{
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Start time has to be before end time!",""));
		}
		else
		{
			InstructorOperations db = new InstructorOperations();
			CourseRequest cr = new CourseRequest(SessionBean.getUserName(),name,-1,prereq,capacity,start,end);
			db.makeCourseRequest(cr);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("Course Request has been sent!",""));
			init();
		}
		//db.makeCourseRequest(SessionBean.getUserName(), name, prereq, capacity, start, end);
		//if(co)
		//	db.makeCourseRequest(SessionBean.getUserName(), coname, prereq, capacity, costart, coend);
		
		
	}
}
