package com.crsys.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import com.crsys.bean.*;
import com.crsys.db.StudentOperations;
@ManagedBean(name="TranscriptView")
public class TranscriptView {

		private List<Integer> terms;
		private List<Grade> transcript;
		private Integer i;
		public Integer getI() {
			return i;
		}

		public List<Grade> getTranscript() {
			return transcript;
		}

		public void setTranscript(List<Grade> transcript) {
			this.transcript = transcript;
		}

		public void setI(Integer i) {
			this.i = i;
		}

		public List<Integer> getTerms() {
			return terms;
		}

		public void setTerms(List<Integer> terms) {
			this.terms = terms;
		}
		@PostConstruct
		public void init(){
			StudentOperations db = new StudentOperations();
			db.initialize();
	        terms = db.getEnrolledTerms(SessionBean.getUserName());	    
		}
		public String select(){
			StudentOperations db = new StudentOperations();
			db.initialize();
			transcript= db.getTranscript(SessionBean.getUserName(), i);
			return "transcript";
		}
}
