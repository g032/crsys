package com.crsys.view;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.crsys.bean.SessionBean;
import com.crsys.bean.User;
import com.crsys.db.RootOperations;

@ManagedBean(name="ModifyAccountView")
@SessionScoped
public class ModifyAccountView {
	private List<User> users;
	private User selectedUser;
	private String u_pass,u_name,u_role;
	public String getU_pass() {
		return u_pass;
	}
	public void setU_pass(String u_pass) {
		this.u_pass = u_pass;
	}
	public String getU_name() {
		return u_name;
	}
	public void setU_name(String u_name) {
		this.u_name = u_name;
	}
	public String getU_role() {
		return u_role;
	}
	public void setU_role(String u_role) {
		this.u_role = u_role;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public User getSelectedUser() {
		return selectedUser;
	}
	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}
	@PostConstruct
	public void init()
	{
		RootOperations db = new RootOperations();
		users=db.getUsers(SessionBean.getUserRole());
		for(int i=0;i<users.size();i++)
		{
			System.out.println(users.get(i));
		}
	}
	public void edit() throws IOException
	{
		//TODO create new xhtml file to edit accounts
		System.out.println("editteyim");
		
		FacesContext.getCurrentInstance().getExternalContext().redirect("editaccount.xhtml");
	}
	public void delete()
	{
		System.out.println(selectedUser.getU_name());
		RootOperations db = new RootOperations();
		db.deleteUser(selectedUser.getU_name());
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Account Deleted",""));
		init();
	}
	public void disable()
	{
		System.out.println(selectedUser.getU_name());
		RootOperations db = new RootOperations();
		db.disableUser(selectedUser.getU_name());
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Account Disabled",""));
		init();
	}
	public void create(){
		
		System.out.println("creating new account");
		RootOperations db = new RootOperations();
		FacesContext context = FacesContext.getCurrentInstance();
		if(!db.isUserExist(u_name))
		{
			db.addUser(u_name, u_pass, u_role);
			context.addMessage(null, new FacesMessage("New Account Created",""));
		}
		else
			context.addMessage(null, new FacesMessage("Please enter unique account id",""));
		init();
		
	}
	public void editAccount(){
		
		System.out.println(selectedUser.getU_name());
		System.out.println(u_pass);
		System.out.println(u_role);
		RootOperations db = new RootOperations();
		db.editUser(selectedUser.getU_name(), u_pass, u_role);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Account Edited",""));
		init();
	}
}
