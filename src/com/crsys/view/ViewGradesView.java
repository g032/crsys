package com.crsys.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.crsys.bean.Course;
import com.crsys.bean.Grade;
import com.crsys.bean.SessionBean;
import com.crsys.db.InstructorOperations;
import com.crsys.db.RootOperations;

@ManagedBean(name="ViewGradesView")
@ViewScoped
public class ViewGradesView {

	private List<Grade> grades=new ArrayList<Grade>();
	private List<Course> courses= new ArrayList<Course>();
	private Grade selectedGrade=new Grade();
	private int index=-1;
	private String u_name,grade;
	private int crn;
	public String getU_name() {
		return u_name;
	}

	public void setU_name(String u_name) {
		this.u_name = u_name;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public int getCrn() {
		return crn;
	}

	public void setCrn(int crn) {
		this.crn = crn;
	}
	
	


	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public List<Grade> getGrades() {
		return grades;
	}

	public void setGrades(List<Grade> grades) {
		this.grades = grades;
	}

	public Grade getSelectedGrade() {
		return selectedGrade;
	}

	public void setSelectedGrade(Grade selectedGrade) {
		this.selectedGrade = selectedGrade;
	}
	public void edit(){
		u_name = selectedGrade.getStudentUname();
		System.out.println(u_name);
		crn= selectedGrade.getCrn();
		grade = selectedGrade.getGrade();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(u_name +" selected!",""));
	}
	public void close(){

	}
	public void modifyGrade(){
		RootOperations db = new RootOperations();
		db.editStudentGrade(u_name, crn, grade);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Grade Entered!",""));
		init();
	}
	
	public void selectCourse(){
		RootOperations db = new RootOperations();
		
			grades=db.viewGrades(index);
	}
	@PostConstruct
	public void init()
	{
		RootOperations rodb= new RootOperations();
		InstructorOperations indb = new InstructorOperations();
		if(SessionBean.getUserRole().equals("ins"))
			courses=indb.getCourses(SessionBean.getUserName());
		else
			courses=rodb.getCourses();
		for(int i=0;i<courses.size();i++)
		{
			System.out.println(courses.get(i));
		}
	}
}

