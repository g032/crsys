package com.crsys.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.crsys.bean.Course;
import com.crsys.bean.User;
import com.crsys.bean.Grade;

public class StudentOperations extends DatabaseOperations {

	public List<Course> getCourses(int crn,String name){
		List<Course> courses = new ArrayList<Course>();
		try{
			initialize();
			String sql;
			sql="SELECT * FROM COURSES WHERE 1=1";
			if(crn!=-1)
				sql+=" AND crn="+crn;
			if(!name.equals(""))
				sql+=" AND name='"+name+"'";
			ps=con.prepareStatement(sql);

			rs=ps.executeQuery();
			while(rs.next()){
				Course c=new Course();
				c.setCrn(rs.getInt("crn"));
				c.setName(rs.getString("name"));
				courses.add(c);
			}    
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return courses;
	}

	public void createApprovalRequest(int crn,String u_name, String explanation) throws Exception{
		try{
			initialize();
			ps=con.prepareStatement("INSERT INTO specialrequest (u_name,c_crn,message) VALUES (?,?,?) ");
			ps.setString(1,u_name);
			ps.setInt(2,crn);
			ps.setString(3,explanation);
			ps.executeUpdate();
		}catch(Exception e){
			throw e;
		}finally{
			finalize();
		}
	}
	public User getInfo(String u_name){

		User ui=null;
		try{
			initialize();

			ps=con.prepareStatement("SELECT * FROM user_info WHERE u_name=?");
			ps.setString(1,u_name);
			rs=ps.executeQuery();
			while(rs.next()){
				ui = new User();
				ui.setName(rs.getString("firstname"));
				ui.setSurname(rs.getString("surname"));
				ui.setAddress(rs.getString("address"));

			}    
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return ui;
	}
	public List<Course> getSchedule(String u_name){
		List<Course> schedule = new ArrayList<Course>();
		try{
			initialize();

			ps=con.prepareStatement("SELECT C.c_crn,C.c_name,C.c_start,C.c_end FROM current_course CT, course C WHERE C.c_crn=CT.c_crn AND CT.u_name=?");
			ps.setString(1,u_name);
			rs=ps.executeQuery();
			while(rs.next()){
				Course c=new Course();
				c.setCrn(rs.getInt("c_crn"));
				c.setName(rs.getString("c_name"));
				c.setStart(rs.getTimestamp("c_start"));
				c.setEnd(rs.getTimestamp("c_end"));
				StudentOperations dao = new StudentOperations();
				c.setRemaining(dao.getRemaining(c.getCrn()));
				schedule.add(c);
			}    
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return schedule;
	}
	public void dropCourse(String u_name,List<Course> crns) throws Exception{
		try{
			Exception exception = new Exception();
			initialize();
			String list="(";
			for(int i=0;i<crns.size();++i)
			{
				list+=Integer.toString(crns.get(i).getCrn())+",";
			}
			list=list.substring(0, list.length()-1)+")";

			ps=con.prepareStatement("SELECT co_crn FROM COREQUISITE WHERE C_CRN IN "+ list);
			rs=ps.executeQuery();
			while(rs.next()){
				int co_crn = rs.getInt("co_crn");
				boolean exist= false;
				for(int i=0;i<crns.size();i++){
					if(crns.get(i).getCrn()==co_crn)
						exist = true;
				}
				if(!exist){
					throw exception;
				}
			}
			for(Course c:crns){
				ps=con.prepareStatement("DELETE FROM CURRENT_COURSE WHERE u_name=? AND c_crn=? ");
				ps.setString(1,u_name);
				ps.setInt(2,c.getCrn());
				ps.executeUpdate();
			}
		}catch(Exception e){
			throw e;
		}finally{
			finalize();
		}
	}
	public void addCourse(List<Course> courses,String u_name) throws Exception{
		Exception exception = new Exception();
		List<Integer> crns = new ArrayList<Integer>();
		try{
			initialize();
			String list="(";
			for(Course c:courses)
			{
				if(getRemaining(c.getCrn())<=0)
					throw exception;

				list+=c.getCrn()+",";
				crns.add(c.getCrn());
			}
			
			list=list.substring(0, list.length()-1)+")";

			ps=con.prepareStatement("SELECT co_crn FROM COREQUISITE WHERE C_CRN IN "+ list);
			rs=ps.executeQuery();
			while(rs.next()){
				int co_crn = rs.getInt("co_crn");
				if(!crns.contains(co_crn)){
					throw exception;
				}
			}
			ps=con.prepareStatement("SELECT * FROM PREREQUISITE WHERE C_CRN IN "+ list);
			rs=ps.executeQuery();
			while(rs.next()){
				int pre_crn = rs.getInt("pre_crn");
				int c_crn=rs.getInt("c_crn");
				if(!hasPermission(u_name,pre_crn,c_crn)){
					throw exception;
				}
			}
			if(checkTimeConflict(courses,u_name)){
				throw exception;
			}

			for(int crn:crns){	
				ps=con.prepareStatement("INSERT INTO CURRENT_COURSE (u_name,c_crn) VALUES (?,?) ");
				ps.setString(1,u_name);
				ps.setInt(2,crn);
				ps.executeUpdate();
			}	

		}catch(Exception e){
			throw e;
		}finally{
			finalize();
		}
	}
	public List<Course> getPrerequisite()
	{
		List<Course> prerequisites = new ArrayList<Course>();
		try{
			initialize();
			ps=con.prepareStatement("SELECT C.c_crn, C.c_name, P.pre_crn FROM prerequisite P,course C WHERE C.c_crn= P.c_crn");
			rs=ps.executeQuery();
			while(rs.next()){
				Course c=new Course();
				c.setCrn(rs.getInt("c_crn"));
				c.setName(rs.getString("c_name"));
				c.setPrerequisite(rs.getInt("pre_crn"));
				prerequisites.add(c);
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}finally{
			finalize();
		}
		return prerequisites;
	}
	public List<Course> getCorequisite()
	{
		List<Course> corequisites = new ArrayList<Course>();
		try{
			initialize();
			ps=con.prepareStatement("SELECT C.c_crn, C.c_name, Co.co_crn FROM corequisite CO,course C WHERE C.c_crn=CO.c_crn");
			rs=ps.executeQuery();
			while(rs.next()){
				Course c=new Course();
				c.setCrn(rs.getInt("c_crn"));
				c.setName(rs.getString("c_name"));
				c.setCorequisite(rs.getInt("co_crn"));
				corequisites.add(c);
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}finally{
			finalize();
		}
		return corequisites;
	}
	public List<Grade> getTranscript(String u_name, Integer term){
		List<Grade> transcript = new ArrayList<Grade>();
		try{
			initialize();

			if(term==-1){
				ps=con.prepareStatement("SELECT T.term_year, T.c_crn, T.grade, C.c_name FROM course C,transcript T WHERE C.c_crn=T.c_crn AND T.u_name=? ORDER BY T.term_year ASC");
				ps.setString(1, u_name);
			}
			else{
				ps=con.prepareStatement("SELECT T.term_year, T.c_crn, T.grade, C.c_name FROM course C,transcript T WHERE C.c_crn=T.c_crn AND T.u_name=? AND T.term_year=? ORDER BY T.c_crn");
				ps.setString(1, u_name);
				ps.setInt(2,term);
			}
			rs=ps.executeQuery();
			while(rs.next()){
				Grade c=new Grade();
				c.setCrn(rs.getInt("c_crn"));
				c.setCoursename(rs.getString("c_name"));
				c.setGrade(rs.getString("grade"));
				c.setTerm(rs.getInt("term_year"));
				transcript.add(c);
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}finally{
			finalize();
		}
		return transcript;
	}
	public List<Integer> getEnrolledTerms(String u_name){
		List<Integer> terms = new ArrayList<Integer>();
		try{
			initialize();

			ps=con.prepareStatement("SELECT DISTINCT(term_year) FROM transcript WHERE u_name=?");
			ps.setString(1, u_name);
			rs=ps.executeQuery();
			while(rs.next()){
				Integer i = rs.getInt("term_year");
				terms.add(i);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}

		return terms;
	}
	private boolean hasPermission(String u_name,int pre_crn,int c_crn){
		try{
			int count=0;
			ps=con.prepareStatement("SELECT COUNT(*) AS MatchCount FROM TRANSCRIPT WHERE U_NAME=? AND C_CRN=?");
			ps.setString(1,u_name);
			ps.setInt(2, pre_crn);
			rs=ps.executeQuery();
			if(rs.next()){
				count=rs.getInt("MatchCount");
			}
			if(count!=0){
				return true;
			}
			else{
				ps=con.prepareStatement("SELECT COUNT(*) AS MatchCount FROM SPECIALAPPROVAL  WHERE U_NAME=? AND C_CRN=?");
				ps.setString(1,u_name);
				ps.setInt(2, c_crn);
				rs=ps.executeQuery();
				if(rs.next()){
					count=rs.getInt("MatchCount");
				}
				if(count!=0){
					return true;
				}
				else{
					return false;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean hasRequest(String u_name,int crn)
	{
		boolean exist=false;
		try{
			initialize();
			ps=con.prepareStatement("SELECT * FROM specialrequest WHERE u_name=? AND c_crn=?");
			ps.setString(1, u_name);
			ps.setInt(2, crn);
			rs=ps.executeQuery();
			if(rs.next()){
				System.out.println("hasrequest debug check it is "+exist);
				exist=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return exist;
	}
	public int getRemaining(int crn){
		int current=0,capacity=0;
		try{
			initialize();
			ps=con.prepareStatement("SELECT COUNT(*) AS Population FROM CURRENT_COURSE  WHERE C_CRN=?");
			ps.setInt(1, crn);
			rs=ps.executeQuery();
			if(rs.next()){
				current=rs.getInt("Population");
			}
			ps=con.prepareStatement("SELECT C_CAPACITY FROM COURSE  WHERE C_CRN=?");
			ps.setInt(1, crn);
			rs=ps.executeQuery();
			if(rs.next()){
				capacity=rs.getInt("C_Capacity");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return capacity-current;
	}
	private boolean checkTimeConflict(List<Course> courses, String u_name){
		for(int i=0;i<courses.size();i++){
			for(int j=i+1;j<courses.size();j++){
				if(((courses.get(i).getStart().getTime()<=courses.get(j).getEnd().getTime())&&
						(courses.get(i).getStart().getTime()>=courses.get(j).getStart().getTime()))||
						((courses.get(i).getEnd().getTime()<=courses.get(j).getEnd().getTime())&&
								(courses.get(i).getEnd().getTime()>=courses.get(j).getStart().getTime()))
						){
					return true;
				}
			}
			try {
				ps=con.prepareStatement("SELECT COUNT(*) AS Conflicting FROM CURRENT_COURSE CC INNER JOIN COURSE C ON C.C_CRN=CC.C_CRN"
						+ " WHERE ((C_START<=? AND C_END>=?) OR (C_START<=? AND C_END>=?)) AND CC.u_name=? ");
				ps.setTimestamp(1,  new java.sql.Timestamp(courses.get(i).getStart().getTime()));
				ps.setTimestamp(2,  new java.sql.Timestamp(courses.get(i).getStart().getTime()));
				ps.setTimestamp(3,  new java.sql.Timestamp(courses.get(i).getEnd().getTime()));
				ps.setTimestamp(4,  new java.sql.Timestamp(courses.get(i).getEnd().getTime()));
				ps.setString(5, u_name);
				System.out.println(ps.toString());
				rs=ps.executeQuery();
				if(rs.next()){
					if(rs.getInt("Conflicting")!=0)
						return true;
				}
			} catch (SQLException e) {

				e.printStackTrace();
			}
			
		}
		return false;
	}
}
