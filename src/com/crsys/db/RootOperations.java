package com.crsys.db;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.crsys.bean.Course;
import com.crsys.bean.CourseRequest;
import com.crsys.bean.Grade;
import com.crsys.bean.User;

public class RootOperations extends DatabaseOperations {
	public void addUser(String userName, String pass,String role){
		try{
			initialize();
			ps=con.prepareStatement("INSERT INTO LOGIN (U_NAME,U_PASS,U_ROLE) VALUES (?,?,?)");
			ps.setString(1, userName);
			ps.setString(2, pass);
			ps.setString(3, role);
			ps.executeUpdate();
			ps=con.prepareStatement("INSERT INTO USER_INFO (u_name,firstname,surname,address) VALUES (?,?,?,?)");
			ps.setString(1, userName);
			ps.setString(2, "NAME");
			ps.setString(3, "SURNAME");
			ps.setString(4, "ADDRESS");
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void editCourse(int c_crn,String c_name,Date c_start,Date c_end)
	{
		try{
			initialize();
			String sql="UPDATE course SET C_CRN="+c_crn+" ";
			if(c_name!=null)
				sql+=", c_name ='"+c_name+"' ";
			if(c_start!=null)
			{
				SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String start= sdf.format(c_start);
				sql+=", c_start='"+start+"' ";
			}
			if(c_end!=null)
			{
				SimpleDateFormat sd = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String end= sd.format(c_end);
				sql+=", c_end='"+end+"' ";
			}
			sql+=" WHERE C_CRN="+c_crn;
			ps=con.prepareStatement(sql);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void registerOverride(String u_name,int crn)
	{
		try{
			initialize();
			ps=con.prepareStatement("INSERT INTO current_course (u_name,c_crn) VALUES (?,?)");
			ps.setString(1, u_name);
			ps.setInt(2,crn);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void deleteUser(String userName){
		try{
			initialize();
			ps=con.prepareStatement("DELETE FROM LOGIN WHERE U_NAME=?");
			ps.setString(1, userName);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void disableUser(String userName){
		try{
			initialize();
			ps=con.prepareStatement("UPDATE LOGIN SET U_ROLE=? WHERE U_NAME=?");
			ps.setString(1, "");
			ps.setString(2, userName);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void editUser(String userName,String pass,String role){
		try{
			initialize();
			String sql="UPDATE LOGIN SET U_NAME="+"'"+userName+"'";
			if(!pass.equals(""))
				sql+=", U_PASS="+"'"+pass+"'";
			if(!role.equals(""))
				sql+=", U_ROLE="+"'"+role+"'";
			
			sql+=" WHERE U_NAME="+"'"+userName+"'";
			ps=con.prepareStatement(sql);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void deleteCourse(int crn)
	{
		try{
			initialize();
			ps=con.prepareStatement("DELETE FROM course WHERE C_CRN=?" );
			ps.setInt(1, crn);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void editStudentGrade(String userName,int crn,String grade){
		try{
			initialize();
			ps=con.prepareStatement("UPDATE CURRENT_COURSE SET GRADE=? WHERE U_NAME=? AND C_CRN=?");
			ps.setString(1, grade);
			ps.setString(2, userName);
			ps.setInt(3, crn);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void addCourse(String userName,int crn,String name){
		try{
			initialize();
			ps=con.prepareStatement("INSERT INTO COURSE (NAME,CRN,INSTRUCTOR) VALUES (?,?,?)");
			ps.setString(1, name);
			ps.setInt(2,crn);
			ps.setString(3, userName);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public List<CourseRequest> getCourseRequests(){
		List<CourseRequest> requests = new ArrayList<CourseRequest>();
		try{
			initialize();
			ps=con.prepareStatement("SELECT * FROM COURSE_REQ");
			rs=ps.executeQuery();
			while(rs.next()){
				CourseRequest cr= new CourseRequest();
				cr.setInstructor(rs.getString("req_ins"));
				cr.setCourseName(rs.getString("c_name"));
				cr.setCorequisiteCRN(rs.getInt("co_crn"));
				cr.setPrerequisiteCRN(rs.getInt("pre_crn"));
				cr.setCapacity(rs.getInt("capacity"));
				cr.setStart(rs.getTimestamp("c_start"));
				cr.setEnd(rs.getTimestamp("c_end"));
				requests.add(cr);
			}
		}catch(Exception e){	
			e.printStackTrace();
		}finally{
			finalize();
		}
		return requests;
	}
	public void rejectRequest(CourseRequest cr){
		try{
			initialize();
			ps=con.prepareStatement("DELETE FROM COURSE_REQ WHERE REQ_INS=? AND C_NAME=? ");
			ps.setString(1,cr.getInstructor());
			ps.setString(2, cr.getCourseName());
			ps.executeUpdate();
			
		}catch(Exception e){	
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void acceptRequest(CourseRequest cr){
		try{
			initialize();
			ps=con.prepareStatement("INSERT INTO COURSE (c_name,c_capacity,c_start,c_end,c_ins) VALUES (?,?,?,?,?)");
			ps.setString(1, cr.getCourseName());
			ps.setInt(2,cr.getCapacity());
			ps.setTimestamp(3,new java.sql.Timestamp(cr.getStart().getTime()));
			ps.setTimestamp(4,new java.sql.Timestamp(cr.getEnd().getTime()));
			ps.setString(5, cr.getInstructor());
			ps.executeUpdate();
			ps=con.prepareStatement("SELECT C_CRN FROM COURSE WHERE C_NAME=?");
			ps.setString(1,cr.getCourseName());
			rs=ps.executeQuery();
			int c_crn=-1;
			if(rs.next())
				c_crn=rs.getInt("c_crn");
			if(cr.getPrerequisiteCRN()!=-1){
				ps=con.prepareStatement("INSERT INTO PREREQUISITE (c_crn,pre_crn) VALUES (?,?)");
				ps.setInt(1, c_crn);
				ps.setInt(2, cr.getPrerequisiteCRN());
				ps.executeUpdate();
			}
			if(cr.getCorequisiteCRN()!=-1){
				ps=con.prepareStatement("INSERT INTO COREQUISITE (c_crn,co_crn) VALUES (?,?)");
				ps.setInt(1, c_crn);
				ps.setInt(2, cr.getCorequisiteCRN());
				ps.executeUpdate();
				ps.setInt(1, cr.getCorequisiteCRN());
				ps.setInt(2, c_crn);
				ps.executeUpdate();
			}
			ps=con.prepareStatement("DELETE FROM COURSE_REQ WHERE REQ_INS=? AND C_NAME=? ");
			ps.setString(1,cr.getInstructor());
			ps.setString(2, cr.getCourseName());
			ps.executeUpdate();
			
		}catch(Exception e){	
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public List<Course> getCourses(){
		List<Course> courses = new ArrayList<Course>();
		try{
			initialize();
			
			ps=con.prepareStatement("SELECT C.*,U.firstname,U.surname FROM course C, user_info U WHERE C.c_ins=U.u_name");
			rs=ps.executeQuery();
			while(rs.next()){
				Course c=new Course();
				c.setStart(rs.getTimestamp("c_start"));
				c.setEnd(rs.getTimestamp("c_end"));
				c.setCrn(rs.getInt("c_crn"));
				c.setIns(rs.getString("firstname")+ " "+ rs.getString("surname"));
				c.setName(rs.getString("c_name"));
				StudentOperations db = new StudentOperations();
				c.setRemaining(db.getRemaining(c.getCrn()));
				courses.add(c);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return courses;
	}
	public List<User> getUsers(String userrole)
	{
		List<User> users = new ArrayList<User>();
		try{
			initialize();
			if(userrole.equals("root")){
			ps=con.prepareStatement("SELECT * FROM login");
			}
			else{
				ps=con.prepareStatement("SELECT * FROM login WHERE u_role!='root' AND u_role!='admin'");
			}
				
			rs=ps.executeQuery();
			while(rs.next()){
				User u=new User();
				u.setU_name(rs.getString("u_name"));
				u.setRole(rs.getString("u_role"));
				if(u.getRole().equals(""))
					u.setRole("disabled");
				u.setPassword(rs.getString("u_pass"));
				users.add(u);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return users;
	}
	public boolean isUserExist(String username)
	{
		boolean exist=false;
		try{
			initialize();
			ps=con.prepareStatement("SELECT * FROM login WHERE u_name=?");
			ps.setString(1, username);
			rs=ps.executeQuery();
			if(rs.next()){
				exist=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return exist;
	}
	public List<Grade> viewGrades(int crn){
		List<Grade> grades = new ArrayList<Grade>();
		try{
			initialize();
			ps=con.prepareStatement("SELECT CC.U_NAME, UI.FIRSTNAME, UI.SURNAME, CC.GRADE FROM  CURRENT_COURSE CC, USER_INFO UI WHERE  CC.U_NAME=UI.U_NAME AND CC.C_CRN=?" );
			ps.setInt(1, crn);
			rs=ps.executeQuery();
			while(rs.next()){
				Grade g = new Grade();
				g.setCrn(crn);
				g.setStudentName(rs.getString("firstname")+" "+rs.getString("surname"));
				g.setStudentUname(rs.getString("u_name"));
				if(rs.getString("grade")==null)
					g.setGrade("NA");
				else
					g.setGrade(rs.getString("grade"));
				grades.add(g);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return grades;
	}
	
}

