package com.crsys.db;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.DriverManager;

public class DatabaseOperations {
		protected ResultSet rs;
		protected Connection con;
		protected PreparedStatement ps;
		
		public DatabaseOperations(){
			rs=null;
			con=null;
			ps=null;
		}
		public void initialize(){
			try {
				Class.forName("com.mysql.jdbc.Driver");
				if(con==null)
					con =DriverManager.getConnection("jdbc:mysql://localhost/crsys","root","1746abcd");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		public void finalize(){
			try {
				if(con!=null)
					con.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public void editInfo(String name,String surname, String address,String u_name){
			try{
				initialize();
				ps=con.prepareStatement("UPDATE USER_INFO SET firstname=?,surname=?,address=? WHERE u_name=?");
				ps.setString(1, name);
				ps.setString(2, surname);
				ps.setString(3, address);
				ps.setString(4, u_name);
				ps.executeUpdate();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				finalize();
			}
		}
	}


