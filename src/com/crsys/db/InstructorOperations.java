package com.crsys.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.crsys.bean.Course;
import com.crsys.bean.CourseRequest;
import com.crsys.bean.Grade;
import com.crsys.bean.SpecialPermission;
import com.crsys.bean.User;

public class InstructorOperations extends DatabaseOperations {
	public void editStudentGrade(String userName,int crn,String grade){
		try{
			initialize();
			ps=con.prepareStatement("UPDATE current_course SET GRADE=? WHERE U_NAME=? AND C_CRN=?");
			ps=con.prepareStatement("UPDATE CURRENT_COURSE SET GRADE=? WHERE U_NAME=? AND C_CRN=?");
			ps.setString(1, grade);
			ps.setString(2, userName);
			ps.setInt(3, crn);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public List<User> getUsers(int crn){
		List<User> users = new ArrayList<User>();
		try{
			initialize();
			//Ask for table names
			ps=con.prepareStatement("SELECT * FROM current_course C WHERE c_crn=?");
			ps.setInt(1,crn);
			rs=ps.executeQuery();
			while(rs.next()){
				User ui = new User();
				ui.setU_name(rs.getString("u_name"));
				users.add(ui);
			}    
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return users;
	}
	public List<SpecialPermission> getPermissionRequests(String u_name){
		List<SpecialPermission> permissions = new ArrayList<SpecialPermission>();
		try{
			initialize();
			ps=con.prepareStatement("SELECT S.u_name, S.c_crn, S.message FROM specialrequest S, course c WHERE C.c_ins=? AND C.c_crn=S.c_crn");
			ps.setString(1, u_name);
			rs=ps.executeQuery();
			while(rs.next()){
				SpecialPermission sp = new SpecialPermission();
				sp.setCrn(rs.getInt("c_crn"));
				sp.setExplanation(rs.getString("message"));
				sp.setStudentUname(rs.getString("u_name"));
				permissions.add(sp);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return permissions;
		
	}
	public void approveRequest(String u_name,int crn){
		try{
			initialize();
			ps=con.prepareStatement("INSERT INTO specialapproval (U_NAME,c_crn) VALUES (?,?)");
			ps.setString(1, u_name);
			ps.setInt(2, crn);
			ps.executeUpdate();
			ps=con.prepareStatement("DELETE FROM specialrequest WHERE U_NAME=? AND C_CRN=?" );
			ps.setString(1, u_name);
			ps.setInt(2, crn);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public void rejectRequest(String u_name,int crn){
		try{
			initialize();
			ps=con.prepareStatement("DELETE FROM specialrequest WHERE U_NAME=? AND C_CRN=?" );
			ps.setString(1, u_name);
			ps.setInt(2, crn);
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
	public List<Grade> viewGrades(int crn){
		List<Grade> grades = new ArrayList<Grade>();
		try{
			initialize();
			ps=con.prepareStatement("SELECT CC.U_NAME,UI.FIRSTNAME,UI.SURNAME,CC.GRADE FROM  CURRENT_COURSE CC INNER JOIN USER_INFO UI ON C.C_CRN=CC.C_CRN AND CC.U_NAME=UI.U_NAME WHERE CC.C_CRN=?" );
			ps.setInt(1, crn);
			rs=ps.executeQuery();
			while(rs.next()){
				Grade g = new Grade();
				g.setCrn(crn);
				g.setStudentName(rs.getString("firstname")+" "+rs.getString("surname"));
				g.setStudentUname(rs.getString("u_name"));
				g.setGrade(rs.getString("grade"));
				grades.add(g);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return grades;
	}
	public List<Course> getCourses(String u_name){
		List<Course> courses = new ArrayList<Course>();
		try{
			initialize();
			
			ps=con.prepareStatement("SELECT * FROM COURSE WHERE c_ins=?");
			ps.setString(1, u_name);
			rs=ps.executeQuery();
			while(rs.next()){
				Course c=new Course();
				c.setCrn(rs.getInt("c_crn"));
				c.setName(rs.getString("c_name"));
				courses.add(c);
			}    
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return courses;
	}
	public void makeCourseRequest(CourseRequest cr) {
		try{
			initialize();	
			ps=con.prepareStatement("INSERT INTO course_req (co_crn,pre_crn,c_name,capacity,c_start,c_end,req_ins) VALUES (?,?,?,?,?,?,?)");
			ps.setInt(1, cr.getCorequisiteCRN());
			ps.setInt(2, cr.getPrerequisiteCRN());
			ps.setString(3, cr.getCourseName());
			ps.setInt(4, cr.getCapacity());
			ps.setTimestamp(5, new java.sql.Timestamp(cr.getStart().getTime()));
			ps.setTimestamp(6, new java.sql.Timestamp(cr.getEnd().getTime()));
			ps.setString(7,cr.getInstructor());
			ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
	}
}
