package com.crsys.db;

public class LoginOperations extends DatabaseOperations {
	public String checkCredentials(String name, String pass){
		String role="";
		try{
			initialize();
			ps=con.prepareStatement("SELECT U_ROLE FROM LOGIN WHERE U_NAME=? AND U_PASS=?");
			ps.setString(1, name);
			ps.setString(2, pass);
			rs=ps.executeQuery();
			if(rs.next()){
				role=rs.getString("u_role");
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			finalize();
		}
		return role;
	}
}
