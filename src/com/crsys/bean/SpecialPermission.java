package com.crsys.bean;

public class SpecialPermission {
	private String explanation;
	private int crn;
	private String studentUname;
	public SpecialPermission(){
		explanation="";
		crn=-1;
	}
	public String getExplanation() {
		return explanation;
	}
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
	public int getCrn() {
		return crn;
	}
	public void setCrn(int crn) {
		this.crn = crn;
	}
	public String getStudentUname() {
		return studentUname;
	}
	public void setStudentUname(String studentUname) {
		this.studentUname = studentUname;
	}
	public String toString()
	{
		return studentUname+" "+crn+" "+explanation;
	}
	
}
