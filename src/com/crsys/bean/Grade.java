package com.crsys.bean;

public class Grade {
	private String studentName;
	private String studentUname;
	private String grade;
	private int crn;
	private String coursename;
	private int term;
	public int getTerm() {
		return term;
	}
	public void setTerm(int term) {
		this.term = term;
	}
	public String getCoursename() {
		return coursename;
	}
	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getStudentUname() {
		return studentUname;
	}
	public void setStudentUname(String studentUname) {
		this.studentUname = studentUname;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public int getCrn() {
		return crn;
	}
	public void setCrn(int crn) {
		this.crn = crn;
	}
	public String toString(){
		return crn+" "+grade+" "+studentUname;
	}
	
}
