package com.crsys.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CourseRequest {
	private String instructor;
	private String courseName;
	private int courseCRN,corequisiteCRN=-1,prerequisiteCRN=-1,capacity=-1;
	private Date start,end;
	
	
	public CourseRequest(String instructor,String courseName,int corequisiteCRN,int prerequisiteCRN,int capacity,Date start,Date end) {
		this.instructor = instructor;
		this.courseName = courseName;
		this.corequisiteCRN = corequisiteCRN;
		this.prerequisiteCRN = prerequisiteCRN;
		this.capacity = capacity;
		this.start = start;
		this.end = end;
	}
	public CourseRequest() {
		// TODO Auto-generated constructor stub
	}
	public String getInstructor() {
		return instructor;
	}
	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public int getCourseCRN() {
		return courseCRN;
	}
	public void setCourseCRN(int courseCRN) {
		this.courseCRN = courseCRN;
	}
	public int getCorequisiteCRN() {
		return corequisiteCRN;
	}
	public void setCorequisiteCRN(int corequisiteCRN) {
		this.corequisiteCRN = corequisiteCRN;
	}
	public int getPrerequisiteCRN() {
		return prerequisiteCRN;
	}
	public void setPrerequisiteCRN(int prerequisiteCRN) {
		this.prerequisiteCRN = prerequisiteCRN;
	}
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public String getEndStr(){
		SimpleDateFormat df = new SimpleDateFormat("EEE HH:mm");
		return df.format(end);
	}
	public String getStartStr(){
		SimpleDateFormat df = new SimpleDateFormat("EEE HH:mm");
		return df.format(start);
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
}
