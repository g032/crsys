package com.crsys.bean;



import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "courseService")
@ApplicationScoped
public class Course {
	private String name;
	private int crn;
	private Date start;
	private Date end;
	private String ins;
	private int remaining;
	private int prerequisite;
	private int corequisite;
	public int getCorequisite() {
		return corequisite;
	}
	public void setCorequisite(int corequisite) {
		this.corequisite = corequisite;
	}
	public int getPrerequisite() {
		return prerequisite;
	}
	public void setPrerequisite(int prerequisite) {
		this.prerequisite = prerequisite;
	}
	public String getStartStr()
	{
		SimpleDateFormat d = new SimpleDateFormat("EEE HH:mm");
		return d.format(start);
	}
	public String getEndStr()
	{
		SimpleDateFormat d = new SimpleDateFormat("EEE HH:mm");
		return d.format(end);
	}
	public int getRemaining() {
		return remaining;
	}
	public void setRemaining(int remaining) {
		this.remaining = remaining;
	}
	public String getIns() {
		return ins;
	}
	public void setIns(String ins) {
		this.ins = ins;
	}
	public Course() {
		name = "";
		crn = -1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCrn() {
		return crn;
	}

	public void setCrn(int crn) {
		this.crn = crn;
	}
	public String toString() { 
	    return crn+ " "+ name;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
}
